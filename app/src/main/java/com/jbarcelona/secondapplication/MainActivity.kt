package com.jbarcelona.secondapplication

import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btnLaunch: Button = findViewById(R.id.btn_launch)
        btnLaunch.setOnClickListener {
            if (isPackageInstalled()) {
                val sendIntent = packageManager.getLaunchIntentForPackage(PACKAGE_NAME)
                startActivity(sendIntent)
                finishAffinity()
            } else {
                Toast.makeText(this, "FirstApplication app is not installed.", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun isPackageInstalled(): Boolean {
        return try {
            packageManager.getPackageInfo(PACKAGE_NAME, 0)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    }

    companion object {
        private const val PACKAGE_NAME = "com.jbarcelona.firstapplication"
    }
}